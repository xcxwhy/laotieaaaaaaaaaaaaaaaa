import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        hasLogin: false,// 是否已经登录
		userId:"",// 用户id
		token:"",// 用户token
		sessionTimeOut:false
    },
    mutations: {
        login(state, sessionData) {
			state.hasLogin = true;
			state.userId = sessionData.userId;
			state.token = sessionData.token;
			state.sessionTimeOut = false;
        },
        logout(state) {
			state.hasLogin = false;
			state.userId = "";
			state.token = "";
			state.sessionTimeOut = true;
        }
    }
})

export default store
