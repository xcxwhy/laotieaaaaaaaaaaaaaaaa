const url_prefix = 'https://laotie.tfxiaochengxu.com/';
var isCheckUnReadMsg = false;
var timer = null;
let HEADER = {
  'Content-Type': 'application/x-www-form-urlencoded',
  };
 const common_utils = require("./common_utils.js");


function updateCheckUnReadMsgFalse(){
	isCheckUnReadMsg = false;
	clearTimeout(timer);
}

function checkUnReadMsg(){
	if(!uni.getStorageSync("xixuan_pintuan_token") || !uni.getStorageSync("xixuan_pintuan_userId")){
		return false;
	}
	if(isCheckUnReadMsg){
		return false;
	}
	isCheckUnReadMsg = true;
	selectUnReadMsg();
}

function selectUnReadMsg(){
	var that = this;
	requestPost("api/user/selectShopBadge", {}, function(res) {
		if(res.code == '301'){// 用户未登录
			return false;
		}
		
		if(Number(res.data.msgCount) > 0){
			uni.setTabBarBadge({
				index:4,
				text: res.data.msgCount+""
			});
			
			// #ifdef APP-PLUS
			plus.runtime.setBadgeNumber(2);
			// #endif
			
			if(res.data.productMsgState == '10'){// 新的消息
				playAudio("/static/mp3/news.mp3");
			}
			if(res.data.orderMsgState == '10'){// 新的订单
				playAudio("/static/mp3/order.mp3");
			}
		}else{
			uni.hideTabBarRedDot({
				index: 4
			});
			// #ifdef APP-PLUS
			plus.runtime.setBadgeNumber(0);
			// #endif
		}
		timer = setTimeout(selectUnReadMsg, 60000);
	},false);
}

// 文件上传封装
// filePath:文件路径，fileType：上传文件类型（image/video/audio）仅支付宝小程序，且必填。，fileName：后台收受对象名称
function uploadSigleFilePost(url,filePath,fileType,fileName,data,succ,showLoad,error){
	let request_data = data;
	if(showLoad){
		uni.showLoading({
			title:" ",
			mask:true
		});
	}
	const baseParam = {
		token:uni.getStorageSync("xixuan_pintuan_token")
	}
	// console.log("requestParam:"+JSON.stringify(Object.assign(data,baseParam)));
	uni.uploadFile({
			url: url_prefix+url,
			filePath: filePath+"",
			fileType: fileType,
			name: fileName,
			formData:Object.assign(data,baseParam),
			sslVerify:false,
			success: (res) => {
				// plus.nativeUI.closeWaiting();
				uni.hideLoading();
				var data = res.data;
				var code = data.code;
				if(data.code == undefined){
					data = JSON.parse(data);
					code = data.code;
				}
				
				if(code == undefined){
					uni.showToast({
						icon: 'none',
						title: "服务器异常，请联系技术",
						position:"bottom"
					});
					return;
				}
				
				if(code == '500'){// 接口请求失败
					var msg = data.msg;
					if(!msg){
						msg = "服务器异常，请联系技术";
					}
					uni.showToast({
						icon: 'none',
						title: msg,
						position:"bottom"
					});
				}else if(code == '4001'){// token 超时
					// #ifdef MP-WEIXIN
						wechat_authLogin(uploadSigleFilePost,[url,filePath,fileType,fileName,request_data,succ,error]);
					// #endif
					// #ifdef APP-PLUS
						phone_authLogin(uploadSigleFilePost,[url,filePath,fileType,fileName,request_data,succ,error]);
					// #endif
					// #ifdef H5
						phone_authLogin(uploadSigleFilePost,[url,filePath,fileType,fileName,request_data,succ,error]);
					// #endif
				}else{
					if(succ){
						succ(data);
					}
				}
			},
			fail: (err) => {
				console.log(err)
				uni.hideLoading();
				uni.showToast({
					icon: 'none',
					title: "网络异常，请稍后重试",
					position:"bottom"
				});
				// uni.showModal({
				// 	content: err.errMsg,
				// 	showCancel: false
				// });
				if(error){
					error();
				}
			}
		});
}
// post 请求封装
function requestPost(url,data,succ,showLoad=false,error){
	let request_data = data;
	if(showLoad){
		uni.showLoading({
			title:"加载中",
			mask:true
		});
	}
	// console.log(uni.getStorageSync("xixuan_pintuan_token")+"=================token")
	const baseParam = {
		token:uni.getStorageSync("xixuan_pintuan_token")
	}
	// console.log("requestParam:"+JSON.stringify(Object.assign(data,baseParam)));
	uni.request({
		url: url_prefix+url,
		method: 'POST',
		data: Object.assign(data,baseParam),
		header:getHeader(),
		sslVerify:false,
		success: res => {
			// console.log("res:============="+JSON.stringify(res.data))
			if(showLoad){
				uni.hideLoading();
			}
			uni.stopPullDownRefresh();
			var code = res.data.code;
			if(res.data.code == undefined){
				var data = JSON.parse(res.data);
				code = data.code;
			}
			
			if(code == undefined){
				var data = JSON.parse(res.data.replace("\\",''));
				uni.showToast({
					icon: 'none',
					title: "服务器异常，请联系技术",
					position:"bottom"
				});
				return;
			}
			if(code == '500'){// 接口请求失败
				
				var msg = res.data.msg;
				if(!msg){
					msg = "服务器异常，请联系技术";
				}
				uni.showToast({
					icon: 'none',
					title: msg,
					position:"bottom"
				});
			}else if(code == '0'){// 接口失败提示
				var msg = res.data.msg;
				if(!msg){
					msg = "服务器异常，请联系技术";
				}
				uni.showToast({
					icon: 'none',
					title: msg,
					position:"bottom"
				});
			}else if(code == '4001'){// token 超时
				// #ifdef MP-WEIXIN
					wechat_authLogin(requestPost,[url,request_data,succ,showLoad,error]);
				// #endif
				// #ifdef APP-PLUS
					phone_authLogin(requestPost,[url,request_data,succ,showLoad,error]);
				// #endif
				// #ifdef H5
					phone_authLogin(requestPost,[url,request_data,succ,showLoad,error]);
				// #endif
			}else{
				if(succ){
					succ(res.data);
				}
			}
		},
		fail: () => {
			uni.stopPullDownRefresh();
			if(showLoad){
				uni.hideLoading();
			}
			uni.showToast({
				icon: 'none',
				title: "网络异常，请稍后重试",
				position:"bottom"
			});
			if(error){
				error();
			}
			// plus.nativeUI.closeWaiting();
		}
	});
}

// post 自动登录
function autoLogin(){
	var token = uni.getStorageSync("xixuan_pintuan_token");
	if(token){
		return;
	}
	var loginName = uni.getStorageSync("xixuan_pintuan_loginName");
	var password = uni.getStorageSync("xixuan_pintuan_password");
	if(loginName && password){// 自动登录
		var url= "api/open/login";
		const data = {
			loginName: loginName,
			loginPassword: password,
			isPassWord: true
		};
		uni.request({
			url: url_prefix+url,
			method: 'POST',
			data: data,
			header:getHeader(),
			sslVerify:false,
			success: res => {
				
				var code = res.data.code;
				if(res.data.code == undefined){
					var data = JSON.parse(res.data);
					code = data.code;
				}
				if(code == '500'){// 自动登录失败
					uni.removeStorageSync("xixuan_pintuan_password");
				}
				if(code == '0'){
					uni.setStorageSync("xixuan_pintuan_token",res.data.data.token);
					checkUnReadMsg();
				}
			}
		});
	}
}
// 检查微信小程序用户授权,手机号登录
function is_wechat_xcx_auth(){
	// #ifdef APP-PLUS || H5
		if(!wx.getStorageSync('xixuan_pintuan_token')){
			uni.navigateTo({
				url:"/pages/login/common_login?is_jump=10"
			})
		}else{
			return true;
		}
	// #endif
	// #ifdef MP-WEIXIN
		// 未完全授权
		if(wx.getStorageSync('xixuan_pintuan_auth') != 2){
		  uni.navigateTo({
			url:"/pages/login/login"
		  })
		}else{
		  return true;
		}
	// #endif
}
// 小程序启动场景
function onStartupScene(query){
	let that = this;
	// 获取场景值
	let scene = query.scene ? common_utils.scene_decode(query.scene) : {};
	// 记录推荐人id
	let uuid = query.uuid ? query.uuid : scene.uuid;
	if(common_utils.is_empty(uuid) && !common_utils.is_empty(scene.custom_params)){
	  uuid = scene.custom_params;
	}
	if(!common_utils.is_empty(uuid)){
	  uni.setStorageSync('xixuan_pintuan_uuid', uuid);
	}
	// 记录商品id
	let goods_id = query.goods_id ? query.goods_id : scene.goods_id;
	if(!common_utils.is_empty(goods_id)){
	  uni.setStorageSync('xixuan_pintuan_goods_id', goods_id);
	}
	// 分享人Id
	let share_openid = query.share_openid ? query.share_openid : '';
	if(share_openid){
		uni.setStorageSync('xixuan_pintuan_share_r_openid', share_openid);
	}
}
/**
 * 微信小程序自动登录
 */
function wechat_authLogin(cb,params){
	if(!params){
	  params = [];
	}
	var that = this;
	let auth_count = uni.getStorageSync('xiaoniu_auth_count');
	if(auth_count > -1){
		auth_count = parseInt(auth_count) + 1;
	}else{
		auth_count = 0;
	}
	uni.setStorageSync('xiaoniu_auth_count',auth_count )
	uni.login({
		success: function (res) {
			var code = res.code;
			let data = {
				code:res.code,
				uuid:uni.getStorageSync("xixuan_pintuan_uuid"),
				share_openid:uni.getStorageSync("xixuan_pintuan_share_r_openid") || '',
				share_uid:uni.getStorageSync("xixuan_pintuan_share_r_uid") || 0
			};
			uni.request({
			  url: url_prefix + 'api.php/login/wechatXcxAuthLogin',
			  method: "POST",
			  header: HEADER,
			  data: data,
			  success: function (res) {
				// 访问成功
				if (res.data.code == 1) {
					uni.setStorageSync('xiaoniu_auth_count', 0);
					uni.removeStorageSync("xixuan_pintuan_uuid");
					uni.removeStorageSync("xixuan_pintuan_share_r_openid");
					uni.removeStorageSync("xixuan_pintuan_share_r_uid");
					// 存储sessionid
					// HEADER.Cookie = 'JSESSIONID=' + res.data.data.token;
					uni.setStorageSync("xixuan_header", HEADER);
					// 用户token
					uni.setStorageSync("xixuan_pintuan_token", res.data.data.token);
					// 小程序是否授权，0：未授权，1：信息授权，2：信息手机号授权
					uni.setStorageSync("xixuan_pintuan_auth", res.data.data.xcx_auth);
					// 用户openid
					uni.setStorageSync("xixuan_pintuan_openid", res.data.data.openid);
					typeof cb == "function" && cb(...params);
				  } else {
					  
					let auth_count = uni.getStorageSync('xiaoniu_auth_count');
					if(!auth_count || auth_count < 3){
						typeof cb == "function" && cb(...params);
					}else{
						// toast("服务器错误，请稍后重试！");
						uni.setStorageSync('xiaoniu_auth_count', 0);
					}
					
				  }
			  }
			})
		},
		fail: function (res) {
		  let auth_count = uni.getStorageSync('xiaoniu_auth_count');
		  if(!auth_count || auth_count < 3){
			wechat_authLogin(cb,params);
		  }else{
			// toast("网络异常，请稍后重试！");
			uni.setStorageSync('xiaoniu_auth_count', 0);
		  }
		  return false;
		}
	});
}

/**
 * 程序心跳
 */
function wechatHeart(){
	requestPost("api.php/user/wechatHeart", {}, function(res) {
		
		let order_message_count = res.data.order_message_count;
		if(order_message_count>0){
			uni.setTabBarBadge({
			  index: 2,
			  text: order_message_count+""
			})
		}else{
			uni.removeTabBarBadge({
				index:2
			});
		}
		
		// 存储平台名称
		uni.setStorageSync('xixuan_plat_name',res.data.plat_name);
		
		// #ifdef APP-PLUS
			plus.runtime.setBadgeNumber(order_message_count);
		// #endif
		
		setTimeout(wechatHeart,5000);
	});
}

/**
 * 手机号自动登录
 * @param {Object} cb
 * @param {Object} params
 */
function phone_authLogin(cb,params){
	if(!params){
	  params = [];
	}
	let phone = uni.getStorageSync("xixuan_pintuan_phone");
	if(!phone){
		return;
	}
	
	var that = this;
	let auth_count = uni.getStorageSync('xiaoniu_auth_count');
	if(auth_count > -1){
		auth_count = parseInt(auth_count) + 1;
	}else{
		auth_count = 0;
	}
	uni.setStorageSync('xiaoniu_auth_count',auth_count );
	
	let data = {
		phone:phone
	};
	uni.request({
	  url: url_prefix + 'api.php/login/phoneAuthLogin',
	  method: "POST",
	  header: HEADER,
	  data: data,
	  success: function (res) {
		// 访问成功
		if (res.data.code == 1) {
			uni.setStorageSync('xiaoniu_auth_count', 0);
			// 存储sessionid
			// HEADER.Cookie = 'JSESSIONID=' + res.data.data.token;
			uni.setStorageSync("xixuan_header", HEADER);
			// 用户token
			uni.setStorageSync("xixuan_pintuan_token", res.data.data.token);
			typeof cb == "function" && cb(...params);
		  } else {
			  
			let auth_count = uni.getStorageSync('xiaoniu_auth_count');
			if(!auth_count || auth_count < 3){
				typeof cb == "function" && cb(...params);
			}else{
				toast("服务器错误，请稍后重试！");
				uni.setStorageSync('xiaoniu_auth_count', 0);
			}
			
		  }
	  }
	})
}

// 获取请求头
function getHeader(){
    let header = uni.getStorageSync("xixuan_header");
    if(header){
      return header;
    }else{
      return HEADER;
    }
  }

function showLoading(text){
	uni.showLoading({
		title:text || " ",
		mask:true
	});
}

//校验数值
function validate_number(val){
	var reg = new RegExp("^(-?\\d+)(\\.\\d{1,2})?$");
	var flag = reg.test(val);
	return flag;
}
//校验邮箱格式
function checkEmail(email){
    return RegExp(/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/).test(email);
}
//校验身份号
function checkIdcard(idcard){
    return RegExp(/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/).test(email);
}
//校验整数
function validate_int(val){
	var reg = new RegExp("^-?\\d+$");
	var flag = reg.test(val);
	return flag;
}

//以什么开头
function startWith(str,targetStr){
	var reg = new RegExp("^"+targetStr);
	var flag = reg.test(str);
	return flag;
}

//以什么结尾
function endWith(str,targetStr){
	var reg = new RegExp(targetStr+"$");
	var flag = reg.test(str);
	return flag;
}

// 产生一个随机数 
function getUid() { 
  return Math.floor(Math.random() * 100000000 + 10000000).toString(); 
}

// 底部吐司消息
function toast(text){
	uni.showToast({
		icon: 'none',
		title: text,
		position:"bottom"
	});
}
// 播放语音
function playAudio(src){
	// var audio=plus.audio.createPlayer(src);
	// audio.play( function () {
	// 	audio.stop();
	// 	// console.log( "Audio play success!" ); 
	// }, function ( e ) {
	// 	console.log( "Audio play error: " + e.message ); 
	// } ); 
	const innerAudioContext = uni.createInnerAudioContext();
	innerAudioContext.autoplay = true;
	innerAudioContext.src = src;
	innerAudioContext.onPlay(() => {
	  // console.log('开始播放');
	});
	innerAudioContext.onError((res) => {
	  console.log(res.errMsg);
	  console.log(res.errCode);
	});
}

// 获取0到1之间的随机小数,num:几位随机小数
function randomDouble(num){
	var random = Math.random();
	while(random == 0 || random< 0.01 || random == 1){
		random = Math.random();
	}
	return random.toFixed(num);
}

function formatTime(time) {
	if (typeof time !== 'number' || time < 0) {
		return time
	}

	var hour = parseInt(time / 3600)
	time = time % 3600
	var minute = parseInt(time / 60)
	time = time % 60
	var second = time

	return ([hour, minute, second]).map(function (n) {
		n = n.toString()
		return n[1] ? n : '0' + n
	}).join(':')
}

function formatLocation(longitude, latitude) {
	if (typeof longitude === 'string' && typeof latitude === 'string') {
		longitude = parseFloat(longitude)
		latitude = parseFloat(latitude)
	}

	longitude = longitude.toFixed(2)
	latitude = latitude.toFixed(2)

	return {
		longitude: longitude.toString().split('.'),
		latitude: latitude.toString().split('.')
	}
}

var dateUtils = {
	UNITS: {
		'年': 31557600000,
		'月': 2629800000,
		'天': 86400000,
		'小时': 3600000,
		'分钟': 60000,
		'秒': 1000
	},
	humanize: function (milliseconds) {
		var humanize = '';
		for (var key in this.UNITS) {
			if (milliseconds >= this.UNITS[key]) {
				humanize = Math.floor(milliseconds / this.UNITS[key]) + key + '前';
				break;
			}
		}
		return humanize || '刚刚';
	},
	format: function (dateStr) {
		var date = this.parse(dateStr)
		var diff = Date.now() - date.getTime();
		if (diff < this.UNITS['天']) {
			return this.humanize(diff);
		}
		var _format = function (number) {
			return (number < 10 ? ('0' + number) : number);
		};
		return date.getFullYear() + '/' + _format(date.getMonth() + 1) + '/' + _format(date.getDay()) + '-' +
			_format(date.getHours()) + ':' + _format(date.getMinutes());
	},
	parse: function (str) { //将"yyyy-mm-dd HH:MM:ss"格式的字符串，转化为一个Date对象
		var a = str.split(/[^0-9]/);
		return new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
	}
};

var validateUtil = {
	provinceAndCitys: {
		11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江",
		31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北",43:"湖南",44:"广东",
		45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",
		65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"
	},
	
	powers: ["7","9","10","5","8","4","2","1","6","3","7","9","10","5","8","4","2"],
	
	parityBit: ["1","0","X","9","8","7","6","5","4","3","2"],
	
	genders: {male:"男",female:"女"},
	
	checkAddressCode: function(addressCode){
		var check = /^[1-9]\d{5}$/.test(addressCode);
		if(!check) return false;
		if(validateUtil.provinceAndCitys[parseInt(addressCode.substring(0,2))]){
			return true;
		}else{
			return false;
		}
	},
	
	checkBirthDayCode: function(birDayCode){
		var check = /^[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))$/.test(birDayCode);
		if(!check) return false;
		var yyyy = parseInt(birDayCode.substring(0,4),10);
		var mm = parseInt(birDayCode.substring(4,6),10);
		var dd = parseInt(birDayCode.substring(6),10);
		var xdata = new Date(yyyy,mm-1,dd);
		if(xdata > new Date()){
			return false;//生日不能大于当前日期
		}else if ( ( xdata.getFullYear() == yyyy ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == dd ) ){
			return true;
		}else{
			return false;
		}
	},
	
	getParityBit: function(idCardNo){
		var id17 = idCardNo.substring(0,17);
		var power = 0;
		for(var i=0;i < 17;i++){
			power += parseInt(id17.charAt(i),10) * parseInt(validateUtil.powers[i]);
		}
		var mod = power % 11;
		return validateUtil.parityBit[mod];
	},
	
	checkParityBit: function(idCardNo){
		var parityBit = idCardNo.charAt(17).toUpperCase();
		if(validateUtil.getParityBit(idCardNo) == parityBit){
			return true;
		}else{
			return false;
		}
	},
	
	checkIdCardNo: function(idCardNo){// 校验身份号
		//15位和18位身份证号码的基本校验
		var check = /^\d{15}|(\d{17}(\d|x|X))$/.test(idCardNo);
		if(!check) return false;
		//判断长度为15位或18位
		if(idCardNo.length==15){
			return validateUtil.check15IdCardNo(idCardNo);
		}else if(idCardNo.length==18){
			return validateUtil.check18IdCardNo(idCardNo);
		}else{
			return false;
		}
	},
	 
	//校验15位的身份证号码
	check15IdCardNo: function(idCardNo){
		//15位身份证号码的基本校验
		var check = /^[1-9]\d{7}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}$/.test(idCardNo);
		if(!check) return false;
		//校验地址码
		var addressCode = idCardNo.substring(0,6);
		check = validateUtil.checkAddressCode(addressCode);
		if(!check) return false;
		var birDayCode = '19' + idCardNo.substring(6,12);
		//校验日期码
		return validateUtil.checkBirthDayCode(birDayCode);
	},
	 
	//校验18位的身份证号码
	check18IdCardNo: function(idCardNo){
		//18位身份证号码的基本格式校验
		var check = /^[1-9]\d{5}[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}(\d|x|X)$/.test(idCardNo);
		if(!check) return false;
		//校验地址码
		var addressCode = idCardNo.substring(0,6);
		check = validateUtil.checkAddressCode(addressCode);
		if(!check) return false;
		//校验日期码
		var birDayCode = idCardNo.substring(6,14);
		check = validateUtil.checkBirthDayCode(birDayCode);
		if(!check) return false;
		//验证校检码
		return validateUtil.checkParityBit(idCardNo);
	},
	 
	formateDateCN: function(day){
		var yyyy =day.substring(0,4);
		var mm = day.substring(4,6);
		var dd = day.substring(6);
		return yyyy + '-' + mm +'-' + dd;
	},
	 
	//获取信息
	getIdCardInfo: function(idCardNo){
		var idCardInfo = {
			gender:"", //性别
			birthday:"" // 出生日期(yyyy-mm-dd)
		};
		if(idCardNo.length==15){
			var aday = '19' + idCardNo.substring(6,12);
			idCardInfo.birthday=validateUtil.formateDateCN(aday);
			if(parseInt(idCardNo.charAt(14))%2==0){
				idCardInfo.gender=validateUtil.genders.female;
			}else{
				idCardInfo.gender=validateUtil.genders.male;
			}
		}else if(idCardNo.length==18){
			var aday = idCardNo.substring(6,14);
			idCardInfo.birthday=validateUtil.formateDateCN(aday);
			if(parseInt(idCardNo.charAt(16))%2==0){
				idCardInfo.gender=validateUtil.genders.female;
			}else{
				idCardInfo.gender=validateUtil.genders.male;
			}
		}
		return idCardInfo;
	},
	
	getId15:function(idCardNo){
		if(idCardNo.length==15){
			return idCardNo;
		}else if(idCardNo.length==18){
			return idCardNo.substring(0,6) + idCardNo.substring(8,17);
		}else{
			return null;
		}
	},
	
	getId18: function(idCardNo){
		if(idCardNo.length==15){
			var id17 = idCardNo.substring(0,6) + '19' + idCardNo.substring(6);
			var parityBit = validateUtil.getParityBit(id17);
			return id17 + parityBit;
		}else if(idCardNo.length==18){
			return idCardNo;
		}else{
			return null;
		}
	},
	checknumber: function(number){//验证护照是否正确
		var str=number;
		//在JavaScript中，正则表达式只能使用"/"开头和结束，不能使用双引号
		var Expression=/(P\d{7})|(G\d{8})/;
		var objExp=new RegExp(Expression);
		if(objExp.test(str)==true){
			return true;
		}else{
			return false;
		}
	},
	stringCheck: function(value){// 字符验证  只能包括中文字、英文字母、数字和下划线
		return /^[\u0391-\uFFE5\w]+$/.test(value);
	},
	byteRangeLength: function(value,param){// 中文字两个字节 请确保输入的值在3-15个字节之间(一个中文字算2个字节)
		var length = value.length;
		for(var i = 0; i < value.length; i++){
			if(value.charCodeAt(i) < 127){
				length++;
			}
		}
		return ( length >= param[0] && length <= param[1] );
	},
	passport: function(value){//护照编号验证 请正确输入您的护照编号
		return validateUtil.checknumber(value);
	},
	isMobile: function(value){// 手机号码验证
		var length = value.length;
		//var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
		var mobile = /^(((1[0-9]{1})|(1[0-9]{1}))+\d{9})$/;
		return length == 11 && mobile.test(value);
	},
	isTel: function(value){// 电话号码验证
		var tel = /^\d{3,4}-?\d{7,9}$/; //电话号码格式010-12345678
		return tel.test(value);
	},
	isPhone: function(value){// 联系电话(手机/电话皆可)验证
		var length = value.length;
		//var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
		var mobile = /^(((1[0-9]{1})|(1[0-9]{1}))+\d{9})$/;
		var tel = /^\d{3,4}-?\d{7,9}$/;
		return tel.test(value) || mobile.test(value);
	},
	isZipCode: function(value){// 邮政编码验证
		var tel = /^[0-9]{6}$/;
		return tel.test(value);
	}
};

/**
 * 获取当前运行场景
 */
function getCurrentSecene(){
	// #ifdef APP-PLUS
		return 'app';
	// #endif
	// #ifdef MP-WEIXIN
		return 'mp-weixin';
	// #endif
	// #ifdef H5
		let ua = window.navigator.userAgent.toLowerCase()
		if (ua.match(/MicroMessenger/i) == 'micromessenger') {
			return 'mp-h5';
		}else{
			return 'h5';
		}
	// #endif
}

/**
 * 页面跳转
 */
function navigateToPage(url,nav_title,is_tip){
	let tabbar_list = [
		'/pages/index/index',
		'/pages/cateGoods/cateGoods',
		'/pages/message/message',
		'/pages/my/my',
	];
	
	if(!url || url == '' || url.trim() == ''){
		if(is_tip){
			this.toast("敬请期待！");
		}
		return;
	}
	
	if(url.indexOf("/pages") == 0){
		// 小程序页面跳转
		let flag = false;
		tabbar_list.forEach(function(e){
			if(url.indexOf(e) == 0){
				flag == true;
			}
		});
		
		if(flag){
			uni.switchTab({
				url:url
			});
		}else{
			if(url.indexOf("?") > -1){
				uni.navigateTo({
					url:url+"&title="+nav_title
				});
			}else{
				uni.navigateTo({
					url:url+"?title="+nav_title
				});
			}
		}
	}else{
		// url 跳转
		uni.navigateTo({
			url:"/pages/openUrl/openUrl?openUrl="+url+"&title="+nav_title
		});
	}
}

module.exports = {
	formatTime: formatTime,
	formatLocation: formatLocation,
	dateUtils: dateUtils,
	requestPost:requestPost,
	validate_number:validate_number,
	toast:toast,
	validate_int:validate_int,
	randomDouble:randomDouble,
	checkEmail:checkEmail,
	uploadSigleFilePost:uploadSigleFilePost,
	playAudio:playAudio,
	showLoading:showLoading,
	validateUtil: validateUtil,
	getUid: getUid,
	autoLogin: autoLogin,
	startWith: startWith,
	endWith: endWith,
	checkUnReadMsg: checkUnReadMsg,
	updateCheckUnReadMsgFalse: updateCheckUnReadMsgFalse,
	wechat_authLogin: wechat_authLogin,
	onStartupScene: onStartupScene,
	is_wechat_xcx_auth: is_wechat_xcx_auth,
	wechatHeart: wechatHeart,
	phone_authLogin:phone_authLogin,
	getCurrentSecene : getCurrentSecene,
	navigateToPage : navigateToPage
}
