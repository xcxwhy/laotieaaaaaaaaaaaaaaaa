module.exports = {
  // 吐司消息提示
  show_toast:function(msg="系统提示",duration=2000){
    uni.showToast({
      title: msg,
      icon:"none",
      duration:duration
    });
  },
  // 加载遮罩
  show_loading:function(msg="加载中..."){
    uni.showLoading({
      title: msg,
      mask:true
    });
  },
  /**
   * 弹出信息
   * @param {String} msg 消息内容
   * @param {String} title 消息标题
   * @param {String} jump_url 确认之后跳转路径
   * @param {String} jump_type 跳转路径类型 
   */
  alert_msg:function(msg="数据错误",title="系统提示",jump_url,jump_type){
    uni.showModal({
      title: title,
        content: msg,
        showCancel: false,
        success:function(){
            if (jump_url){
                if (jump_type == 'nav'){
                    uni.navigateTo({url: jump_url});
                } else if (jump_type == 'tab'){
                    uni.switchTab({ url: jump_url});
                }
            }
        }
    });
  },
  // 判断是否是空
  is_empty:function(str){
    if(str){
      if(str == undefined){
        return true;
      }else if(str == 'undefined'){
        return true;
      }else if(!str.trim()){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  },
  // 是否存在数组中
  is_in_array:function(value,array){
    return array.indexOf(value) > -1;
  },
 
  // scene 解码
  scene_decode:function(e){
    if (e === undefined)
      return {};
    let scene = decodeURIComponent(e),
      params = scene.split(','),
      data = {};
    for (let i in params) {
      var val = params[i].split(':');
      val.length > 0 && val[0] && (data[val[0]] = val[1] || null)
    }
    return data;
  },
  // 对象转URL
  urlEncode:function(data){
    var _result = [];
    for (var key in data) {
      var value = data[key];
      if (value.constructor == Array) {
        value.forEach(_value => {
          _result.push(key + "=" + _value);
        });
      } else {
        _result.push(key + '=' + value);
      }
    }
    return _result.join('&');
  },
  _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
  // base64 解密
  base64_decode: function(input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    while (i < input.length) {
        enc1 = this._keyStr.indexOf(input.charAt(i++));
        enc2 = this._keyStr.indexOf(input.charAt(i++));
        enc3 = this._keyStr.indexOf(input.charAt(i++));
        enc4 = this._keyStr.indexOf(input.charAt(i++));
        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;
        output = output + String.fromCharCode(chr1);
        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }
    } return this._utf8to16(output);
  },
  // base64 加密
  base64_encode:function(str) { 
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;
    str = this._utf16to8(str);
    while (i < str.length) {
      chr1 = str.charCodeAt(i++);
      chr2 = str.charCodeAt(i++);
      chr3 = str.charCodeAt(i++);
      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;
      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }
      output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
    } return output;
  },
  _utf16to8: function(str) {
    var out, i, len, c;
    out = "";
    len = str.length;
    for(i = 0; i < len; i++) {
        c = str.charCodeAt(i);
        if ((c >= 0x0001) && (c <= 0x007F)) {
            out += str.charAt(i);
        } else if (c > 0x07FF) {
            out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
            out += String.fromCharCode(0x80 | ((c >> 6) & 0x3F));
            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
        } else {
            out += String.fromCharCode(0xC0 | ((c >> 6) & 0x1F));
            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
        }
    }
    return out;
  },
  _utf8to16:function(str) { 
    var out, i, len, c;
    var char2, char3;
    out = "";
    len = str.length;
    i = 0;
    while(i < len) {
        c = str.charCodeAt(i++);
        switch(c >> 4)
        {
            case 0: case 1: case 2: case 3: case 4: case 5: case 6:case7:
              out += str.charAt(i-1);
            break;
            case 12: case 13:
              char2 = str.charCodeAt(i++);
              out += String.fromCharCode(((c & 0x1F) << 6) | (char2&0x3F));
            break;
            case 14:
              char2 = str.charCodeAt(i++);
              char3 = str.charCodeAt(i++);
              out += String.fromCharCode(((c & 0x0F) << 12) |
                ((char2 & 0x3F) << 6) |
                ((char3 & 0x3F) << 0));
            break;
        }
    } 
    return out;
  },
  /**
   * 生成二维码
    let QR =  this.getCode();
    QR.then(res => {
     
    });
   */
  createCode:function(text){
    return new Promise(function (resolve, reject) {
      new QRCode('myQrcode',{
        text: text,
        width: 200,
        height: 200,
        padding: 12, // 生成二维码四周自动留边宽度，不传入默认为0
        correctLevel: QRCode.CorrectLevel.L, // 二维码可辨识度
        callback: (res) => {
          resolve(res.path)
        }
      });
    });
  },
  // 判断是否是数字
  is_int:function(obj){
    if(obj == null){
      return false;
    }else{
      let reg=/^[1-9]\d*$/;
      return reg.test(obj);
    }
  },
  // 获取今天日期
  getTodayDate:function(){
    var myDate = new Date();
    let month = myDate.getMonth();
    let day = myDate.getDate();
    month = month == 11 ? 12 : month + 1;
    month = month > 9 ? month : '0'+month;
    day = day > 9 ? day : '0'+day;
    return myDate.getFullYear() + "-" + month + "-" + day;
  },
  // 获取昨天日期
  getYesDate:function(){
    var day = new Date();
    day.setDate(day.getDate() - 1);
    return this.dateFormat(day,"yyyy-MM-dd");
  },
  // 获取1小时以后
  getOneHourLaterTime:function(){
    var day = new Date();
    day.setHours(day.getHours() + 1);
    return this.dateFormat(day,"yyyy-MM-dd h:m:s");
  },
  // 获取3小时以后
  getThreeHourLaterTime:function(){
    var day = new Date();
    day.setHours(day.getHours() + 3);
    return this.dateFormat(day,"yyyy-MM-dd h:m:s");
  },
  // 日期格式化
  dateFormat:function(date,fmt){
      var o = {
      "M+": date.getMonth() + 1, //月份
      "d+": date.getDate(), //日
      "h+": date.getHours(), //小时
      "m+": date.getMinutes(), //分
      "s+": date.getSeconds(), //秒
      "q+": Math.floor((date.getMonth() + 3) / 3), //季度
      "S": date.getMilliseconds() //毫秒
      };
      if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
      for (var k in o)
      if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
      return fmt;
  },
  // 是否是合法数字
  is_number:function(obj){
    if(obj == null){
      return false;
    }else{
      let reg=/^(([1-9][0-9]{0,7})|0)+(.[0-9]{1,2})?$/;
      return reg.test(obj);
    }
  },
  // 时间格式化
  formatTime:function(date) {
    if (typeof date == 'string' || 'number') {
      try {
        date = date.replace(/-/g, '/')//兼容ios
      } catch (error) {
      }
      date = new Date(date)
    }
  
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()
    
    return {
      str: [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':'),
      arr: [year, month, day, hour, minute, second]
    }
  },
  // 时间补零
  formatNumber:function (n) {
    n = n.toString()
    return n[1] ? n : '0' + n
  }
}